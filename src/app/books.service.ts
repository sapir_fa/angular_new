import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class BooksService {

  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary: "blabla1"},
  {title:'War and Peace', author:'Leo Tolstoy', summary: "blabla2"},
  {title:'The Magic Mountain', author:'Thomas Mann', summary: "blabla3"}];

  public addBooks(){
    setInterval(()=>this.books.push({title:'A new one',author:'New aouthor',summary:'Short sammary'}))
  }


  public getBooks(){
    const booksObservable = new Observable(observer => {
      setInterval(()=>observer.next(this.books),500)
    });
    return booksObservable;
  } 

  /*
  public getBooks(){
    return this.books;
  } 
*/

  constructor() { }
}
